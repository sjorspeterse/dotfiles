import os
import platform
import sys

def create_simlink(file_name):
    # Find the settings file in the same directory as this script
    SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
    VS_SETTINGS_SRC_PATH = os.path.join(SCRIPT_DIR, file_name)

    # https://code.visualstudio.com/docs/getstarted/settings#_settings-file-locations
    if platform.system() == 'Windows':
        VS_SETTINGS_DST_PATH = os.path.expandvars(r"%APPDATA%\\Code\\User\\" + file_name)
    elif platform.system() == "Darwin":
        VS_SETTINGS_DST_PATH = os.path.expandvars(r"$HOME/Library/Application Support/Code/User/" + file_name)
    elif platform.system() == "Linux":
        VS_SETTINGS_DST_PATH = os.path.expandvars(r"$HOME/.config/Code/User/" + file_name)
    else:
        print('platform "', platform.system(), '" is not implemented')
        sys.exit(1)

    # On Windows, there might be a symlink pointing to a different file
    # Always remove symlinks
    if os.path.islink(VS_SETTINGS_DST_PATH):
        os.remove(VS_SETTINGS_DST_PATH)

    choice = input('symlink %r -> %r ? (y/n) ' % (VS_SETTINGS_SRC_PATH, VS_SETTINGS_DST_PATH))
    if choice == 'y':
        try:
            os.symlink(VS_SETTINGS_SRC_PATH, VS_SETTINGS_DST_PATH)
            print('Done')
        except Exception as e:
            print('FAIL!', e)
    else:
        print('aborted')

if __name__ == '__main__':
    create_simlink('settings.json')
    create_simlink('keybindings.json')
    input()
